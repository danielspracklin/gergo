/* Good on you for modifying your layout! if you don't have
 * time to read the QMK docs, a list of keycodes can be found at
 *
 * https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
 *
 * There's also a template for adding new layers at the bottom of this file!
 */

#include QMK_KEYBOARD_H

#define BASE 0 // default layer
#define EXTEND 1
#define FUNCTIONS 2
#define QWERT 3


// Blank template at the bottom

enum customKeycodes {
	URL  = 1
};

/* Keymap template
 *
 * ,-------------------------------------------.                         ,-------------------------------------------.
 * |        |      |      |      |      |      |                         |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|------.           .------|------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|------|           |------|------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |      |      |      |      |      |      |        |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *                        .------.   .------.                                 .------.   .-----.
 *                        |      |   |      |                                 |      |   |     |
 *                        '------'   '------'                                 `------.   '-----'
 *                                        ,-------.       ,-------.
 *                                        |       |       |       |
 *                                 ,------|-------|       |-------|------.
 *                                 |      |       |       |       |      |
 *                                 |      |       |       |       |      |
 *                                 |      |       |       |       |      |
 *                                 `--------------'       `--------------'
 */

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[BASE] = LAYOUT_gergo(
    KC_TAB,    KC_Q,  KC_W, KC_F, KC_P, KC_G,																KC_J,                   KC_L, KC_U, KC_Y, KC_SCOLON, KC_BSLASH,
		KC_ESCAPE, KC_A,  KC_R, KC_S, KC_T, KC_D, KC_LBRACKET,									KC_RBRACKET,            KC_H, KC_N, KC_E, KC_I, KC_O, KC_QUOTE,
		KC_LSPO,   KC_Z,  KC_X, KC_C, KC_V, KC_B, KC_LBRACKET, LGUI(KC_SPC),		OSM(MOD_LSFT|MOD_LALT),	KC_RBRACKET, KC_K, KC_M, KC_COMMA, KC_DOT, KC_SLASH, KC_RSPC,
		MO(2),     MO(1), LGUI_T(KC_SPC), KC_BSPC,															MO(1),									LALT_T(KC_ENT), OSM(MOD_LCTL|MOD_LALT), KC_TRNS
		),

[EXTEND] = LAYOUT_gergo(
    KC_TRNS, KC_ESCAPE,       KC_WH_U, KC_WBAK, KC_WFWD, KC_MS_U,										KC_PGUP, KC_HOME, KC_UP,   KC_END,  KC_DEL,  KC_TRNS,
    KC_TRNS, OSM(MOD_LSFT),   KC_WH_D, KC_LCTL, KC_LALT, KC_MS_D, KC_TRNS, 				  KC_TRNS, KC_PGDN, KC_LEFT, KC_DOWN, KC_RGHT, KC_BSPC,		KC_TRNS,
    KC_TRNS, KC_UNDO, 				KC_CUT,  KC_COPY, KC_PSTE, KC_NO, 	KC_TRNS, KC_TRNS,	KC_TRNS, KC_TRNS, KC_BTN1, KC_BTN2, KC_MS_L, KC_MS_R,		KC_NO,		KC_TRNS,
    KC_TRNS, KC_TRNS, 				KC_TRNS, KC_TRNS,																			KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS
    ),

[FUNCTIONS] = LAYOUT_gergo(
	KC_NO,		KC_NO,   	KC_F7,	KC_F8, KC_F9, KC_F10, 									KC_GRAVE, KC_7,			KC_8,			KC_9, KC_NO,	TG(3),
	KC_NO,		KC_NO,   	KC_F4,	KC_F5, KC_F6, KC_F11, KC_NO,  				  KC_NO,		KC_EQUAL, KC_4,			KC_5, KC_6,		KC_NO,	CG_SWAP,
	KC_LSFT,	KC_NO,		KC_F1,	KC_F2, KC_F3, KC_F12, KC_NO, KC_TRNS,		KC_TRNS,	KC_NO,		KC_MINUS, KC_1, KC_2,		KC_3,		KC_NO,		KC_RSFT,
	KC_TRNS,	KC_TRNS,	KC_TRNS, KC_TRNS,																KC_TRNS,	KC_TRNS,	KC_0,			KC_DOT
    ),

[QWERT] = LAYOUT_gergo(
	KC_NO, 		KC_Q,		KC_W, KC_E, KC_R, KC_T, 								KC_Y,		KC_U,		KC_I,	KC_O, KC_P,			KC_NO,
	KC_NO, 		KC_A, 	KC_S, KC_D, KC_F, KC_G, KC_NO,					KC_NO,	KC_H,		KC_J, KC_K, KC_L,			KC_SCOLON,	KC_NO,
	KC_LSPO,	KC_Z,		KC_X, KC_C, KC_V, KC_B, KC_NO, KC_NO, 	KC_NO,	KC_NO,	KC_N, KC_M, KC_COMMA, KC_DOT,			KC_SLASH, KC_RSPC,
	KC_NO,		KC_NO,	LGUI_T(KC_SPC), KC_BSPC,								MO(1),	LALT_T(KC_ENT), KC_NO, TO(0)
		)
};
